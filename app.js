//modules
var path = require('path');
var https = require('https');
var express = require('express');
var fs = require('fs');

//express object
var app = express();

//credentials for https
var credentials = {
	key: fs.readFileSync('key.pem'),
	cert: fs.readFileSync('cert.pem')
};

var PORT = 5678;

app.use(express.static(path.join(__dirname, 'public')));
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(PORT);
console.log('server running: https://localhost:' + PORT + ' (Ctrl+C to Quit)');

// Handle Ctrl-C (graceful shutdown)
process.on('SIGINT', function() {
  console.log('Exiting...');
  process.exit(0);
});
