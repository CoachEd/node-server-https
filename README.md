# Sample Node Server

This is a sample web server written in Node.js. For Monmouth.

## Getting Started

1. Clone this repository
1. Download and install [Node.js](https://nodejs.org/en/)
1. Install the required Node.js modules: go into the node-server directory and run `npm install`
1. The server runs on port 1234. Modify app.js and restart, to change the port number.

### Running the Server

`node app.js`

### Testing the server

Go to https://localhost:1234/
